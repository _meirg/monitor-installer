#!/bin/bash
#author: meirg
#email: 0504140q0q@gmail.com

set -e

help(){
    echo  "Usage: install monitoring tool Prometheus, Node-exporter, Grafana
    -p  set specific Prometheus version in format v1.2.3, the default is latest.
    -r  set Prometheus storage retention in format 1h | 17h | 3d.
    -n  set specific Node-exporter version in format v1.2.3, the default is latest.
    -g  set specific Grafana version in format 1.2.3, the default is latest.
    -s  set service, prometheus | node-exporter | grafana (only for docker-compose)
    
    examples:
    ./monitor_installer.sh -p v1.8.0 -g 7.2.0
    ./monitor_installer.sh -n v5.0.2 -r 2d
    ./monitor_installer.sh -s prometheus -r 4h
    "
    exit 1
}

while getopts p:r:n:g:s:h option
     do case "${option}" in
        p)prom_version=${OPTARG};;
        r)prom_retention=${OPTARG};;
        n)exporter_version=${OPTARG};;
        g)grafana_version=${OPTARG};;
        s)service=${OPTARG};;
        *) help
    esac
done

unique_name=$RANDOM
TYPE='tsdb'
NETWORK=monitor
BASE=$(cd ${0%/*}; pwd)/task
mkdir -p $BASE/grafana/provisioning/{datasources,dashboards}

set_retention(){
    # set retention config for different version
    if [[ ${prom_version:0:1} != 1 ]] ; then
        echo "--storage.tsdb.retention.time="
    else
        TYPE=local
        echo "--storage.local.retention="
    fi
}

run_exporter(){
    docker run -d \
        -p 9100:9100 \
        --name "node-exporter-${unique_name}" \
        --network $NETWORK \
        --restart always \
        -v "/:/host:ro,rslave" \
        quay.io/prometheus/node-exporter:${exporter_version:-latest} \
        --path.rootfs=/host
}

run_prometheus(){
    docker run -d \
        -p 9090:9090 \
        --network $NETWORK \
        --name "prometheus-${unique_name}" \
        --restart always \
        -v $BASE/prometheus.yml:/etc/prometheus/prometheus.yml \
        prom/prometheus:${prom_version:-latest} \
        --config.file=/etc/prometheus/prometheus.yml \
        --storage.tsdb.path=/prometheus \
        $(set_retention)${prom_retention:-1h} \
        --web.console.libraries=/usr/share/prometheus/console_libraries \
        --web.console.templates=/usr/share/prometheus/consoles
}

run_grafana(){
    docker run -d \
    --network $NETWORK \
    --name "grafana-${unique_name}" \
    --restart always \
    -p 3000:3000 \
    -v $BASE/grafana/provisioning/:/etc/grafana/provisioning/ \
    grafana/grafana:${grafana_version:-latest}
}


cat << END > $BASE/prometheus.yml
global:
  scrape_interval:    5s
  external_labels: 
    monitor: 'codelab-monitor' 
scrape_configs: 
  - job_name: 'prometheus' 
    scrape_interval: 10s 
    static_configs: 
      - targets: ['localhost:9090']
  - job_name: 'node_exporter' 
    scrape_interval: 10s 
    static_configs: 
      - targets: ['node-exporter:9100']
END

cat << END > $BASE/grafana/provisioning/datasources/prometheus.yml
deleteDatasources:
  - name: Prometheus
    orgId: 1
apiVersion: 1
datasources:
  - name: Prometheus
    type: prometheus
    access: proxy
    uid: my_unique_uid
    url: http://prometheus:9090
    basicAuth: false
    isDefault: true
    version: 1
    editable: true
END

cat << END > $BASE/grafana/provisioning/dashboards/Prometheus_Monitoring.json
{
  "annotations": {
    "list": [
      {
        "builtIn": 1,
        "datasource": "-- Grafana --",
        "enable": true,
        "hide": true,
        "iconColor": "rgba(0, 211, 255, 1)",
        "name": "Annotations & Alerts",
        "type": "dashboard"
      }
    ]
  },
  "editable": true,
  "gnetId": null,
  "graphTooltip": 0,
  "id": 3,
  "links": [],
  "panels": [
    {
      "aliasColors": {},
      "bars": false,
      "dashLength": 10,
      "dashes": false,
      "datasource": null,
      "fieldConfig": {
        "defaults": {
          "custom": {}
        },
        "overrides": []
      },
      "fill": 1,
      "fillGradient": 0,
      "gridPos": {
        "h": 8,
        "w": 12,
        "x": 0,
        "y": 0
      },
      "hiddenSeries": false,
      "id": 8,
      "legend": {
        "avg": false,
        "current": false,
        "max": false,
        "min": false,
        "show": true,
        "total": false,
        "values": false
      },
      "lines": true,
      "linewidth": 1,
      "nullPointMode": "null",
      "options": {
        "alertThreshold": true
      },
      "percentage": false,
      "pluginVersion": "7.2.2",
      "pointradius": 2,
      "points": false,
      "renderer": "flot",
      "seriesOverrides": [],
      "spaceLength": 10,
      "stack": false,
      "steppedLine": false,
      "targets": [
        {
          "expr": "prometheus_engine_query_duration_seconds",
          "interval": "",
          "legendFormat": "",
          "refId": "A"
        }
      ],
      "thresholds": [],
      "timeFrom": null,
      "timeRegions": [],
      "timeShift": null,
      "title": "Prometheus query duration",
      "tooltip": {
        "shared": true,
        "sort": 0,
        "value_type": "individual"
      },
      "type": "graph",
      "xaxis": {
        "buckets": null,
        "mode": "time",
        "name": null,
        "show": true,
        "values": []
      },
      "yaxes": [
        {
          "format": "short",
          "label": null,
          "logBase": 1,
          "max": null,
          "min": null,
          "show": true
        },
        {
          "format": "short",
          "label": null,
          "logBase": 1,
          "max": null,
          "min": null,
          "show": true
        }
      ],
      "yaxis": {
        "align": false,
        "alignLevel": null
      }
    },
    {
      "aliasColors": {},
      "bars": false,
      "dashLength": 10,
      "dashes": false,
      "datasource": null,
      "fieldConfig": {
        "defaults": {
          "custom": {}
        },
        "overrides": []
      },
      "fill": 1,
      "fillGradient": 0,
      "gridPos": {
        "h": 8,
        "w": 12,
        "x": 12,
        "y": 0
      },
      "hiddenSeries": false,
      "id": 2,
      "legend": {
        "avg": false,
        "current": false,
        "max": false,
        "min": false,
        "show": true,
        "total": false,
        "values": false
      },
      "lines": true,
      "linewidth": 1,
      "nullPointMode": "null",
      "options": {
        "alertThreshold": true
      },
      "percentage": false,
      "pluginVersion": "7.2.2",
      "pointradius": 2,
      "points": false,
      "renderer": "flot",
      "seriesOverrides": [],
      "spaceLength": 10,
      "stack": false,
      "steppedLine": false,
      "targets": [
        {
          "expr": "rate(node_cpu_seconds_total[5m])",
          "interval": "",
          "legendFormat": "",
          "refId": "A"
        }
      ],
      "thresholds": [],
      "timeFrom": null,
      "timeRegions": [],
      "timeShift": null,
      "title": "Node CPU",
      "tooltip": {
        "shared": true,
        "sort": 0,
        "value_type": "individual"
      },
      "type": "graph",
      "xaxis": {
        "buckets": null,
        "mode": "time",
        "name": null,
        "show": true,
        "values": []
      },
      "yaxes": [
        {
          "format": "short",
          "label": null,
          "logBase": 1,
          "max": null,
          "min": null,
          "show": true
        },
        {
          "format": "short",
          "label": null,
          "logBase": 1,
          "max": null,
          "min": null,
          "show": true
        }
      ],
      "yaxis": {
        "align": false,
        "alignLevel": null
      }
    },
    {
      "aliasColors": {},
      "bars": false,
      "dashLength": 10,
      "dashes": false,
      "datasource": null,
      "fieldConfig": {
        "defaults": {
          "custom": {}
        },
        "overrides": []
      },
      "fill": 1,
      "fillGradient": 0,
      "gridPos": {
        "h": 8,
        "w": 12,
        "x": 0,
        "y": 8
      },
      "hiddenSeries": false,
      "id": 12,
      "legend": {
        "avg": false,
        "current": false,
        "max": false,
        "min": false,
        "show": true,
        "total": false,
        "values": false
      },
      "lines": true,
      "linewidth": 1,
      "nullPointMode": "null",
      "options": {
        "alertThreshold": true
      },
      "percentage": false,
      "pluginVersion": "7.2.2",
      "pointradius": 2,
      "points": false,
      "renderer": "flot",
      "seriesOverrides": [],
      "spaceLength": 10,
      "stack": false,
      "steppedLine": false,
      "targets": [
        {
          "expr": "prometheus_tsdb_blocks_loaded",
          "interval": "",
          "legendFormat": "",
          "refId": "A"
        }
      ],
      "thresholds": [],
      "timeFrom": null,
      "timeRegions": [],
      "timeShift": null,
      "title": "Prometheus tsdb blocks loaded",
      "tooltip": {
        "shared": true,
        "sort": 0,
        "value_type": "individual"
      },
      "type": "graph",
      "xaxis": {
        "buckets": null,
        "mode": "time",
        "name": null,
        "show": true,
        "values": []
      },
      "yaxes": [
        {
          "format": "short",
          "label": null,
          "logBase": 1,
          "max": null,
          "min": null,
          "show": true
        },
        {
          "format": "short",
          "label": null,
          "logBase": 1,
          "max": null,
          "min": null,
          "show": true
        }
      ],
      "yaxis": {
        "align": false,
        "alignLevel": null
      }
    },
    {
      "aliasColors": {},
      "bars": false,
      "dashLength": 10,
      "dashes": false,
      "datasource": null,
      "fieldConfig": {
        "defaults": {
          "custom": {}
        },
        "overrides": []
      },
      "fill": 1,
      "fillGradient": 0,
      "gridPos": {
        "h": 8,
        "w": 12,
        "x": 12,
        "y": 8
      },
      "hiddenSeries": false,
      "id": 6,
      "legend": {
        "avg": false,
        "current": false,
        "max": false,
        "min": false,
        "show": true,
        "total": false,
        "values": false
      },
      "lines": true,
      "linewidth": 1,
      "nullPointMode": "null",
      "options": {
        "alertThreshold": true
      },
      "percentage": false,
      "pluginVersion": "7.2.2",
      "pointradius": 2,
      "points": false,
      "renderer": "flot",
      "seriesOverrides": [],
      "spaceLength": 10,
      "stack": false,
      "steppedLine": false,
      "targets": [
        {
          "expr": "node_hwmon_temp_celsius",
          "interval": "",
          "legendFormat": "",
          "refId": "A"
        }
      ],
      "thresholds": [],
      "timeFrom": null,
      "timeRegions": [],
      "timeShift": null,
      "title": "Node temp",
      "tooltip": {
        "shared": true,
        "sort": 0,
        "value_type": "individual"
      },
      "type": "graph",
      "xaxis": {
        "buckets": null,
        "mode": "time",
        "name": null,
        "show": true,
        "values": []
      },
      "yaxes": [
        {
          "format": "short",
          "label": null,
          "logBase": 1,
          "max": null,
          "min": null,
          "show": true
        },
        {
          "format": "short",
          "label": null,
          "logBase": 1,
          "max": null,
          "min": null,
          "show": true
        }
      ],
      "yaxis": {
        "align": false,
        "alignLevel": null
      }
    },
    {
      "aliasColors": {},
      "bars": false,
      "dashLength": 10,
      "dashes": false,
      "datasource": null,
      "fieldConfig": {
        "defaults": {
          "custom": {}
        },
        "overrides": []
      },
      "fill": 1,
      "fillGradient": 0,
      "gridPos": {
        "h": 8,
        "w": 12,
        "x": 0,
        "y": 16
      },
      "hiddenSeries": false,
      "id": 10,
      "legend": {
        "avg": false,
        "current": false,
        "max": false,
        "min": false,
        "show": true,
        "total": false,
        "values": false
      },
      "lines": true,
      "linewidth": 1,
      "nullPointMode": "null",
      "options": {
        "alertThreshold": true
      },
      "percentage": false,
      "pluginVersion": "7.2.2",
      "pointradius": 2,
      "points": false,
      "renderer": "flot",
      "seriesOverrides": [],
      "spaceLength": 10,
      "stack": false,
      "steppedLine": false,
      "targets": [
        {
          "expr": "prometheus_http_response_size_bytes_sum",
          "interval": "",
          "legendFormat": "",
          "refId": "A"
        }
      ],
      "thresholds": [],
      "timeFrom": null,
      "timeRegions": [],
      "timeShift": null,
      "title": "Prometheus http response size",
      "tooltip": {
        "shared": true,
        "sort": 0,
        "value_type": "individual"
      },
      "type": "graph",
      "xaxis": {
        "buckets": null,
        "mode": "time",
        "name": null,
        "show": true,
        "values": []
      },
      "yaxes": [
        {
          "format": "short",
          "label": null,
          "logBase": 1,
          "max": null,
          "min": null,
          "show": true
        },
        {
          "format": "short",
          "label": null,
          "logBase": 1,
          "max": null,
          "min": null,
          "show": true
        }
      ],
      "yaxis": {
        "align": false,
        "alignLevel": null
      }
    },
    {
      "aliasColors": {},
      "bars": false,
      "dashLength": 10,
      "dashes": false,
      "datasource": null,
      "fieldConfig": {
        "defaults": {
          "custom": {}
        },
        "overrides": []
      },
      "fill": 1,
      "fillGradient": 0,
      "gridPos": {
        "h": 8,
        "w": 12,
        "x": 12,
        "y": 16
      },
      "hiddenSeries": false,
      "id": 4,
      "legend": {
        "avg": false,
        "current": false,
        "max": false,
        "min": false,
        "show": true,
        "total": false,
        "values": false
      },
      "lines": true,
      "linewidth": 1,
      "nullPointMode": "null",
      "options": {
        "alertThreshold": true
      },
      "percentage": false,
      "pluginVersion": "7.2.2",
      "pointradius": 2,
      "points": false,
      "renderer": "flot",
      "seriesOverrides": [],
      "spaceLength": 10,
      "stack": false,
      "steppedLine": false,
      "targets": [
        {
          "expr": "node_disk_read_bytes_total",
          "interval": "",
          "legendFormat": "",
          "refId": "A"
        }
      ],
      "thresholds": [],
      "timeFrom": null,
      "timeRegions": [],
      "timeShift": null,
      "title": "Node disk read",
      "tooltip": {
        "shared": true,
        "sort": 0,
        "value_type": "individual"
      },
      "type": "graph",
      "xaxis": {
        "buckets": null,
        "mode": "time",
        "name": null,
        "show": true,
        "values": []
      },
      "yaxes": [
        {
          "format": "short",
          "label": null,
          "logBase": 1,
          "max": null,
          "min": null,
          "show": true
        },
        {
          "format": "short",
          "label": null,
          "logBase": 1,
          "max": null,
          "min": null,
          "show": true
        }
      ],
      "yaxis": {
        "align": false,
        "alignLevel": null
      }
    }
  ],
  "schemaVersion": 26,
  "style": "dark",
  "tags": [],
  "templating": {
    "list": []
  },
  "time": {
    "from": "now-12h",
    "to": "now"
  },
  "timepicker": {},
  "timezone": "",
  "title": "Task Out",
  "uid": "WpnHNlpGk",
  "version": 1
}
END

cat << END > $BASE/grafana/provisioning/dashboards/dashboard.yml
apiVersion: 1
providers:
- name: 'Prometheus'
  orgId: 1
  folder: ''
  type: file
  disableDeletion: false
  editable: true
  options:
    path: /etc/grafana/provisioning/dashboards
END

create_docker-compose(){
    cat << END > $BASE/docker-compose.yml
    version: '3.7'

    networks:
        front-tier:
        back-tier:

    services:
        prometheus:
            image: prom/prometheus:${prom_version:-latest}
            volumes:
            - $BASE/prometheus.yml/:/etc/prometheus/prometheus.yml
            command:
            - '--config.file=/etc/prometheus/prometheus.yml'
            - '--storage.$TYPE.path=/prometheus'
            - '--web.console.libraries=/usr/share/prometheus/console_libraries'
            - '--web.console.templates=/usr/share/prometheus/consoles'
            - $(set_retention)${prom_retention:-1h}
            ports:
            - 9090:9090
            networks:
            - back-tier
            restart: always

        node-exporter:
            image: prom/node-exporter:${exporter_version:-latest}
            volumes:
            - /proc:/host/proc:ro
            - /sys:/host/sys:ro
            - /:/rootfs:ro
            command: 
            - '--path.procfs=/host/proc' 
            - '--path.sysfs=/host/sys'
            - --collector.filesystem.ignored-mount-points
            - "^/(sys|proc|dev|host|etc|rootfs/var/lib/docker/containers|rootfs/var/lib/docker/overlay2|rootfs/run/docker/netns|rootfs/var/lib/docker/aufs)($$|/)"
            ports:
            - 9100:9100
            networks:
            - back-tier
            restart: always

        grafana:
            image: grafana/grafana:${grafana_version:-latest}
            depends_on:
            - prometheus
            ports:
            - 3000:3000
            volumes:
            - $BASE/grafana/provisioning/:/etc/grafana/provisioning/
            networks:
            - back-tier
            - front-tier
            restart: always
END
}

# cheke if docker-compose instaled in system, if not install the monitor stack with docker command 
if [[ $(whereis docker-compose) == */docker-compose* ]]; then
    create_docker-compose
    yes | docker-compose -f $BASE/docker-compose.yml stop $service  &> /dev/null
    yes | docker-compose -f $BASE/docker-compose.yml rm $service  &> /dev/null
    docker-compose -f $BASE/docker-compose.yml up -d $service
else
    if [[ $(docker network ls --filter "name=$NETWORK" | wc -l) == 1 ]]; then 
      docker network create $NETWORK 1> /dev/null
    fi
    exporter_container_id=$(run_exporter $exporter_version)
    prometheus_container_id=$(run_prometheus)
    grafana_container_id=$(run_grafana)
fi

echo -e "\nInstallation finished, the websites will be available in a few seconds\n"  
echo "Prometheus:    http://localhost:9090"
echo "Node Exporter: http://localhost:9100"
echo "Grafana:       http://localhost:3000"
