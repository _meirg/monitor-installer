Usage: install monitoring tool Prometheus, Node-exporter, Grafana

    -p  set specific Prometheus version in format v1.2.3, the default is latest. 
    -r  set Prometheus storage retention in format 1h | 17h | 3d. 
    -n  set specific Node-exporter version in format v1.2.3, the default is latest. 
    -g  set specific Grafana version in format 1.2.3, the default is latest.
    -s  set service, prometheus | node-exporter | grafana (only for docker-compose) 

examples:
```
./monitor_installer.sh -p v1.8.0 -g 7.2.0
./monitor_installer.sh -n v5.0.2 -r 2d
./monitor_installer.sh -s prometheus -r 4h
```

